package client

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
)

// Client is the instance of client connect to
type Client interface {
	Post(url string, data interface{})
}

var _ Client = &SAMClient{}

const ()

type SAMClient struct {
}

func (s *SAMClient) Post(url string, data interface{}) {
	var body []byte

	if strData, ok := data.(string); ok {
		body = []byte(strData)
	} else if byteData, ok := data.([]byte); ok {
		body = byteData
	} else {
		body, _ = json.Marshal(data)
	}

	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(body))
	if err != nil {
		logrus.Error(err)
		return
	}

	// Build headers.
	req.Header.Add("X-Token-API", "28e1fc3f8fd3a4367c422e0c73f9aba6")
	req.Header.Add("X-Authentication", "SamSamAgent")

	client := buildClient()

	resp, err := client.Do(req)
	if err != nil {
		logrus.Errorln(err)
		return
	}

	if resp == nil {
		return
	}

	if resp.StatusCode != http.StatusCreated && resp.StatusCode != http.StatusOK {
		logrus.Errorf("status code %d not good.\n", resp.StatusCode)
	}

	defer resp.Body.Close()

	responseBody, err := io.ReadAll(resp.Body)
	if err != nil {
		logrus.Errorln(err)
		return
	}

	logrus.Infof("Response Body: %s\n", string(responseBody))
}

func buildClient() (client *http.Client) {
	client = &http.Client{
		Timeout: time.Second * 10,
	}

	return
}
