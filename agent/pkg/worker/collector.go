// package worker

// import (
// 	"encoding/json"
// 	"image/jpeg"
// 	"io/ioutil"
// 	"os"

// 	"github.com/luanldt/simpleagent/cmd/agent/wmic"
// 	"github.com/luanldt/simpleagent/pkg/client"
// 	"github.com/luanldt/simpleagent/pkg/models"
// 	"github.com/sirupsen/logrus"
// )

// type Collector struct {
// 	UUID string
// }

// // Verify that Collector implements BaseWork
// var _ BaseWorker = &Collector{}

// func (c *Collector) New(UUID string) BaseWorker {
// 	return &Collector{
// 		UUID: UUID,
// 	}
// }

// func (c *Collector) Run() {
// 	logrus.Infof("Begin %s...", "Collector")

// 	var wmic wmic.WMIC

// 	info := &models.ReqAgent{}
// 	info.Applications = wmic.CollectApplicationInstalled()
// 	info.Startups = wmic.CollectStartup()
// 	info.Antiviruses = wmic.CollectAV()
// 	info.SoundDevices = wmic.CollectSoundDevice()
// 	info.DiskDrives = wmic.CollectDiskDrive()
// 	info.NICs = wmic.CollectNIC()
// 	info.MemoryChips = wmic.CollectMemoryChip()
// 	info.Desktops = wmic.CollectDesktop()
// 	info.OS = wmic.CollectOS()
// 	info.Volumes = wmic.CollectVolume()
// 	info.ComputerSystem = wmic.CollectComputerSystem()
// 	info.CPUs = wmic.CollectCPU()

// 	info.Id = c.UUID

// 	str, err := json.Marshal(info)
// 	if err != nil {
// 		logrus.Error(err)
// 	}

// 	var client client.SAMClient
// 	client.Post(c.UUID, str)

// 	c.doScreenshoot(wmic)

// 	logrus.Infof("End %s...", "Collector")
// }

// func (c *Collector) doCommand() {
// 	doRunCommandSequence(strUUID, response.Commands[:])
// }

// func (c *Collector) doCollectResource() {
// 	resource := wmic.CollectResourceStat()

// 	postResource(strUUID, resource)
// }

// func (c *Collector) doScreenshoot(wmic wmic.WMIC) {
// 	image, err := wmic.CaptureScreen()
// 	if err != nil {
// 		logrus.Error(err)
// 		return
// 	}

// 	file, err := ioutil.TempFile(os.TempDir(), "s")
// 	if err != nil {
// 		logrus.Error(err)
// 		return
// 	}

// 	defer file.Close()
// 	defer os.Remove(file.Name())

// 	opt := jpeg.Options{
// 		Quality: 90,
// 	}

// 	err = jpeg.Encode(file, image, &opt)
// 	if err != nil {
// 		logrus.Error(err)
// 		return
// 	}

// 	postFile(c.UUID, file.Name())
// }
