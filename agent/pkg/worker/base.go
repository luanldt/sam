package worker

type BaseWorker interface {
	New(cron string) BaseWorker

	Run()
}
