package models

import "github.com/luanldt/simpleagent/cmd/agent/wmic"

type ReqAgent struct {
	Id             string              `json:"_id"`
	Applications   []wmic.Application  `json:"applications"`
	Startups       []wmic.Startup      `json:"startups"`
	Antiviruses    []wmic.Antivirus    `json:"antiviruses"`
	SoundDevices   []wmic.SoundDevice  `json:"soundDevices"`
	DiskDrives     []wmic.DiskDrive    `json:"diskDrives"`
	NICs           []wmic.NIC          `json:"nics"`
	MemoryChips    []wmic.MemoryChip   `json:"memoryChips"`
	ComputerSystem wmic.ComputerSystem `json:"computerSystem"`
	OS             wmic.OS             `json:"os"`
	CPUs           []wmic.CPU          `json:"cpus"`
	Desktops       []wmic.Desktop      `json:"desktops"`
	Volumes        []wmic.Volume       `json:"volumes"`
}

type Command struct {
	Command string `json:"command"`
	UUID    string `json:"uuid"`
}

type ResAgent struct {
	Commands []Command `json:"commands"`
}
