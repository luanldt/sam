package wmic

import (
	"bufio"
	"bytes"
	"os/exec"
	"reflect"
	"strconv"
	"strings"
	"sync"
)

// Collect application installed in PC using wmic product get.
func (s *WMIC) CollectApplicationInstalled() []Application {
	var command = "wmic product get /format:list"
	return collectMultiple[Application](command)
}

func (s *WMIC) CollectStartup() []Startup {
	var command = "wmic startup get /format:list"
	return collectMultiple[Startup](command)
}

func (s *WMIC) CollectAV() []Antivirus {
	var command = `wmic /Namespace:\\root\SecurityCenter2 path AntivirusProduct get /format:list`
	return collectMultiple[Antivirus](command)
}

func (s *WMIC) CollectSoundDevice() []SoundDevice {
	var command = "wmic sounddev get /format:list"
	return collectMultiple[SoundDevice](command)
}

func (s *WMIC) CollectDiskDrive() []DiskDrive {
	var command = "wmic diskdrive get /format:list"
	return collectMultiple[DiskDrive](command)
}

func (s *WMIC) CollectNIC() []NIC {
	var command = "wmic nic get /format:list"
	return collectMultiple[NIC](command)
}

func (s *WMIC) CollectMemoryChip() []MemoryChip {
	var command = "wmic memorychip get /format:list"
	return collectMultiple[MemoryChip](command)
}

func (s *WMIC) CollectPartition() []Partition {
	var command = "wmic partition get /format:list"
	return collectMultiple[Partition](command)
}

func (s *WMIC) CollectOS() OS {
	var command = "wmic os get /format:list"
	return collectSingle[OS](command)
}

func (s *WMIC) CollectDesktop() []Desktop {
	var command = "wmic desktop get /format:list"
	return collectMultiple[Desktop](command)
}

func (s *WMIC) CollectVolume() []Volume {
	var command = "wmic volume get /format:list"
	return collectMultiple[Volume](command)

}

func (s *WMIC) CollectComputerSystem() ComputerSystem {
	var command = "wmic computersystem get /format:list"
	return collectSingle[ComputerSystem](command)
}

func (s *WMIC) CollectCPU() []CPU {
	var command = "wmic cpu get /format:list"
	return collectMultiple[CPU](command)
}

func (s *WMIC) CollectResourceStat() (res ResourcesStat) {
	var wg sync.WaitGroup
	wg.Add(3)
	go func() {
		defer wg.Done()
		loadPercentageOutput := doCommand(`wmic`, `cpu`, `get`, `LoadPercentage`, `/format:value`)
		scanner := bufio.NewScanner(bytes.NewBuffer([]byte(loadPercentageOutput)))
		for scanner.Scan() {
			line := scanner.Text()

			indexSeparator := strings.Index(line, "=")

			if indexSeparator == -1 {
				continue
			}

			// key
			key := line[:indexSeparator]

			// value
			value := line[indexSeparator+1 : len(line)-1]

			if len(key) > 0 && key == "LoadPercentage" {
				intVal, _ := strconv.ParseFloat(value, 32)
				res.LoadPercentage = float32(intVal)
			}
		}
	}()

	go func() {
		defer wg.Done()
		loadPercentageOutput := doCommand(`wmic`, `os`, `get`, `FreePhysicalMemory`, `/format:value`)
		scanner := bufio.NewScanner(bytes.NewBuffer([]byte(loadPercentageOutput)))
		for scanner.Scan() {
			line := scanner.Text()

			indexSeparator := strings.Index(line, "=")

			if indexSeparator == -1 {
				continue
			}

			// key
			key := line[:indexSeparator]

			// value
			value := line[indexSeparator+1 : len(line)-1]

			if len(key) > 0 && key == "FreePhysicalMemory" {
				intVal, _ := strconv.Atoi(value)
				res.FreePhysicalMemory = intVal
			}
		}
	}()

	go func() {
		defer wg.Done()
		loadPercentageOutput := doCommand(`wmic`, `os`, `get`, `TotalVisibleMemorySize`, `/format:value`)
		scanner := bufio.NewScanner(bytes.NewBuffer([]byte(loadPercentageOutput)))
		for scanner.Scan() {
			line := scanner.Text()

			indexSeparator := strings.Index(line, "=")

			if indexSeparator == -1 {
				continue
			}

			// key
			key := line[:indexSeparator]

			// value
			value := line[indexSeparator+1 : len(line)-1]

			if len(key) > 0 && key == "TotalVisibleMemorySize" {
				intVal, _ := strconv.Atoi(value)
				res.TotalPhysicalMemory = intVal
			}
		}
	}()

	wg.Wait()

	return
}

func collectSingle[T any](command string) (document T) {
	commandSplited := strings.Split(command, " ")

	output := doCommand(commandSplited[0], commandSplited[1:]...)

	scanner := bufio.NewScanner(bytes.NewBuffer([]byte(output)))

	var rec = new(T)
	for scanner.Scan() {
		line := scanner.Text()

		indexSeparator := strings.Index(line, "=")

		if indexSeparator == -1 {
			continue
		}

		// if the previous line is empty and this line has data we will add the current record to the array result & new record.

		// key
		key := line[:indexSeparator]

		// value
		value := line[indexSeparator+1 : len(line)-1]

		if len(key) > 0 {
			valueRec := reflect.ValueOf(rec).Elem()
			field := valueRec.FieldByName(key)
			if field.CanSet() {
				if field.Type().Kind() == reflect.String {
					field.SetString(value)
				}
			}
		}

		document = *rec
	}
	return
}

// collect multiple
func collectMultiple[T any](command string) (documents []T) {
	commandSplited := strings.Split(command, " ")

	output := doCommand(commandSplited[0], commandSplited[1:]...)

	scanner := bufio.NewScanner(bytes.NewBuffer([]byte(output)))

	var rec = new(T)
	var emptyLinePrevios = true
	for scanner.Scan() {
		line := scanner.Text()

		indexSeparator := strings.Index(line, "=")

		if indexSeparator == -1 {
			if !emptyLinePrevios {
				documents = append(documents, *rec)
			}
			emptyLinePrevios = true
			continue
		}

		// if the previous line is empty and this line has data we will create new record.
		if emptyLinePrevios {
			rec = new(T)
			emptyLinePrevios = false
		}

		// key
		key := line[:indexSeparator]

		// value
		value := line[indexSeparator+1 : len(line)-1]

		if len(key) > 0 {
			valueRec := reflect.ValueOf(rec).Elem()
			field := valueRec.FieldByName(key)
			if field.CanSet() {
				if field.Type().Kind() == reflect.String {
					field.SetString(value)
				}
			}
		}
	}

	return
}

// Run a command & get the output
func doCommand(name string, args ...string) string {

	// run command
	command := exec.Command(name, args...)

	// get the output
	output, err := command.Output()

	// check error
	if err != nil {
		// log.Fatal(err)
	}

	// convert to string & return
	return string(output)
}
