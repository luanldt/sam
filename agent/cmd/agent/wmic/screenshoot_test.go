package wmic

import (
	"image/jpeg"
	"os"
	"testing"
)

func TestCaptureScreen(t *testing.T) {
	wmic := WMIC{}
	image, err := wmic.CaptureScreen()

	f, err := os.Create("abc.jpg")
	if err != nil {

	}

	defer f.Close()

	opt := jpeg.Options{
		Quality: 90,
	}

	err = jpeg.Encode(f, image, &opt)
	if err != nil {

	}
}
