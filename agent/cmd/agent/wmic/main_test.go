package wmic

import (
	"log"
	"reflect"
	"testing"
)

type App struct {
	Type string
}

func TestDoCommand(t *testing.T) {
	var app App = App{}
	v := reflect.ValueOf(&app).Elem()
	v.FieldByName("Type").SetString("Abc")
	expected := "Abc"
	if expected != app.Type {
		t.Errorf("Expected %s do not match actual %s", expected, app.Type)
	}
}

func TestCollectResourceStat(t *testing.T) {
	wmic := &WMIC{}
	res := wmic.CollectResourceStat()

	log.Println(res)

	if res.FreePhysicalMemory == 0 {
		t.Errorf("Expect `FreePhysicalMemory` should has value")
	}

	if res.LoadPercentage == 0 {
		t.Errorf("Expect `LoadPercentage` should has value")
	}

	if res.TotalPhysicalMemory == 0 {
		t.Errorf("Expect `TotalPhysicalMemory` should has value")
	}
}
