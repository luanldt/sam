package filesystem

import (
	"log"
	"os"
	"testing"
)

func TestTree(t *testing.T) {
	var fs FS
	json, _ := fs.Tree("C:\\Program Files (x86)")
	f, _ := os.Create("abx.json")
	f.WriteString(json)
}

func setupTestDownloadFileAtPath(tb testing.TB) func(tb testing.TB) {
	log.Println("setup test")

	return func(tb testing.TB) {
		log.Println("teadown test")
	}
}

func TestDownloadFileAtPath(t *testing.T) {
	var fs FS
	bs, err := fs.DownloadFileAtPath("C:\\Users\\lnguyen.larion\\Desktop\\NetMotionPaaS10\\Application.json")
	if err != nil {
		log.Panic(err)
	}
	f, err := os.Create("abc.json")
	f.Write(bs)
	defer f.Close()
}
