package filesystem

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"
)

type File struct {
	Name         string
	IsDir        bool
	Path         string
	Size         int64
	IsLink       bool
	LinksTo      string
	ModifiedTime time.Time
	Childrens    []*File
}

type FS struct{}

// Walk all files in the child of a given path.
// Then return a JSON string is a structure of directories.
// Work like `tree` command windows.
func (f *FS) Tree(path string) (string, error) {
	if len(path) == 0 {
		return "", errors.New("path is empty")
	}

	var stack []*File
	pathOSFile, _ := os.Stat(path)
	pathFile := toFile(pathOSFile, path)
	stack = append(stack, pathFile)
	for len(stack) > 0 {
		file := stack[len(stack)-1]
		stack = stack[:len(stack)-1]
		children, _ := ioutil.ReadDir(file.Path)
		for i := 0; i < len(children); i++ {
			child := (toFile(children[i], filepath.Join(file.Path, children[i].Name())))
			file.Childrens = append(file.Childrens, child)
			stack = append(stack, child)
		}
	}
	output, error := json.Marshal(pathFile)
	if error != nil {
		return "", error
	}
	return string(output), nil
}

func toFile(file os.FileInfo, path string) *File {
	f := File{
		Name:         file.Name(),
		IsDir:        file.IsDir(),
		Path:         path,
		Size:         file.Size(),
		ModifiedTime: file.ModTime(),
		Childrens:    []*File{},
	}

	if file.Mode()&os.ModeSymlink == os.ModeSymlink {
		f.IsLink = true
		f.LinksTo, _ = filepath.EvalSymlinks(filepath.Join(path, file.Name()))
	}

	return &f
}

// Download a file at a given path.
// Then return []byte.
// It may not work properly with the large file.
func (f *FS) DownloadFileAtPath(path string) ([]byte, error) {
	if len(path) == 0 {
		return nil, errors.New("path is empty")
	}

	b, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	return b, nil
}
