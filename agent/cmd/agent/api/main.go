package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/sirupsen/logrus"
)

type RESTClient interface {
	// Do a method POST
	Post(URI string, headers map[string]string, data interface{}) string

	// Do a method POST file with fieldName & list file
	PostFile(URI string, headers map[string]string, field string, files []string)

	// // Do a method POST
	// Get(URI string) string
}

type MockAPIClient struct {
}

func (s *MockAPIClient) Post(URI string, headers map[string]string, data interface{}) string {

	// check type is string

	var strBody []byte
	if _, ok := data.(string); ok {
		strBody = []byte(fmt.Sprintf("%v", data))
	} else {
		var err *error
		strBody, *err = json.Marshal(data)
		checkErr(*err)
	}

	req, err := http.NewRequest(http.MethodPost, URI, bytes.NewBuffer(strBody))

	ignoreErr(err)

	if len(headers) > 0 {
		for key, val := range headers {
			req.Header.Add(key, val)
		}
	}

	client := getClient()

	resp, err := client.Do(req)

	ignoreErr(err)

	if resp != nil {

		if resp.StatusCode != http.StatusCreated && resp.StatusCode != http.StatusOK {
			fmt.Printf("Status code not good. %d\n", resp.StatusCode)
		}

		defer resp.Body.Close()

		bodyBytes, err := io.ReadAll(resp.Body)
		ignoreErr(err)

		return string(bodyBytes)
	}

	return ""
}

func (s *MockAPIClient) PostFile(URI string, headers map[string]string, field string, files []string) {
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	file := files[0]
	fw, err := writer.CreateFormFile(field, filepath.Base(file))
	checkErr(err)

	f, err := os.Open(file)
	checkErr(err)

	io.Copy(fw, f)
	defer f.Close()

	writer.Close()

	req, err := http.NewRequest(http.MethodPost, URI, body)
	checkErr(err)

	if len(headers) > 0 {
		for key, val := range headers {
			req.Header.Add(key, val)
		}
	}

	req.Header.Set("Content-Type", writer.FormDataContentType())

	client := getClient()

	resp, err := client.Do(req)

	ignoreErr(err)

	if resp != nil {

		if resp.StatusCode != http.StatusCreated && resp.StatusCode != http.StatusOK {
			fmt.Printf("Status code not good. %d\n", resp.StatusCode)
		}

		defer resp.Body.Close()

		bodyBytes, err := io.ReadAll(resp.Body)
		ignoreErr(err)

		logrus.Print(string(bodyBytes))
	}

	// return ""
}

func getClient() (client *http.Client) {
	client = &http.Client{
		Timeout: time.Second * 10,
	}

	return
}

func ignoreErr(e error) {
	if e != nil {
		logrus.Info(e)
	}
}

func checkErr(e error) {
	if e != nil {
		logrus.Panic(e)
	}
}
