package main

import (
	"encoding/json"
	"fmt"
	"image/jpeg"
	"io/ioutil"
	"path"
	"strings"
	"sync"
	"time"

	"os"
	"os/exec"
	_ "os/exec"

	"github.com/luanldt/simpleagent/cmd/agent/api"
	"github.com/luanldt/simpleagent/cmd/agent/wmic"
	"github.com/robfig/cron/v3"
	log "github.com/sirupsen/logrus"

	"github.com/google/uuid"
	"github.com/kardianos/service"
)

const HOST_ADDR = "http://192.168.146.1:3000"

// Post string data to BE.
func post(data string) Response {

	var restClient api.RESTClient = &api.MockAPIClient{}

	var headers = map[string]string{
		"x-apikey":     "547f517c893bdbcec0127de706a50d3465706",
		"Content-Type": "application/json",
	}

	// resp := restClient.Post("https://hhcvgns0x9.execute-api.us-east-1.amazonaws.com/dev/api/v1/agent", headers, data)
	resp := restClient.Post(fmt.Sprintf("%s/api/v1/agent", HOST_ADDR), headers, data)

	var response Response
	json.Unmarshal([]byte(resp), &response)

	return response
}

func postFile(uuid string, file string) {
	var restClient api.RESTClient = &api.MockAPIClient{}

	var headers = map[string]string{
		"x-apikey": "547f517c893bdbcec0127de706a50d3465706",
	}

	restClient.PostFile(fmt.Sprintf("%s/api/v1/agent/%s/screenshoot", HOST_ADDR, uuid), headers, "image", []string{file})
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

type Data struct {
	Id             string              `json:"_id"`
	Applications   []wmic.Application  `json:"applications"`
	Startups       []wmic.Startup      `json:"startups"`
	Antiviruses    []wmic.Antivirus    `json:"antiviruses"`
	SoundDevices   []wmic.SoundDevice  `json:"soundDevices"`
	DiskDrives     []wmic.DiskDrive    `json:"diskDrives"`
	NICs           []wmic.NIC          `json:"nics"`
	MemoryChips    []wmic.MemoryChip   `json:"memoryChips"`
	ComputerSystem wmic.ComputerSystem `json:"computerSystem"`
	OS             wmic.OS             `json:"os"`
	CPUs           []wmic.CPU          `json:"cpus"`
	Desktops       []wmic.Desktop      `json:"desktops"`
	Volumes        []wmic.Volume       `json:"volumes"`
}

type Response struct {
	Commands []Command `json:"commands"`
}

type Command struct {
	Command string `json:"command"`
	UUID    string `json:"uuid"`
}

type ResourceStatPost struct {
	CPUPercent    float32 `json:"cpuPercent"`
	MemoryPercent float32 `json:"memoryPercent"`
	MemoryTotal   int     `json:"memoryTotal"`
	MemoryUsed    int     `json:"memoryUsed"`
	Time          int64   `json:"time"`
}

type CommandResponsePost struct {
	UID        string `json:"uid"`
	Response   string `json:"response"`
	ResponseAt int64  `json:"responseAt"`
	Done       bool   `json:"done"`
}

const KEY_LUPDATED_IDENFIED = "LUpdateIdentified"

// Init function when start application with check exists key UUID in Registry
func initApp() (id string) {

	// find program data folder windows
	programData := os.Getenv("ProgramData")

	// open file
	f, err := os.OpenFile(path.Join(programData, KEY_LUPDATED_IDENFIED), os.O_RDWR, os.FileMode.Perm(0644))

	// check err open file
	if err != nil {
		log.Fatal(err)

		if os.IsNotExist(err) {
			newUUID, _ := uuid.NewRandom()
			content := newUUID.String()
			f.WriteString(content)
		}
	}

	// get file info to check size content file is empty
	fileInfo, e := f.Stat()
	check(e)

	// check content
	if fileInfo.Size() == 0 {
		newUUID, _ := uuid.NewRandom()
		content := newUUID.String()
		f.WriteString(content)
		id = content
	} else {
		b := make([]byte, 36)
		n, e := f.Read(b)
		check(e)
		id = string(b[:n])
	}

	// validate if uuid not correct then rewrite file.
	u, e := uuid.Parse(id)
	if e != nil {
		newUUID, _ := uuid.NewRandom()
		content := newUUID.String()
		f.WriteAt([]byte(content), 0)
		id = content
	} else {
		log.Debugf("%s uuid is correct", u)
	}

	// read content
	defer f.Close()

	return
}

func doRunCommandSequence(strUUID string, commands []Command) {
	if len(commands) == 0 {
		return
	}

	var responseCommands []CommandResponsePost

	for _, line := range commands {
		commandSplited := strings.Split(strings.Trim(line.Command, ""), " ")
		// run command
		command := exec.Command(commandSplited[0], commandSplited[1:]...)

		// get the output
		output, err := command.Output()
		var response string
		if err != nil {
			log.Error(err)
			response = fmt.Sprintf("%s", err)
		} else {
			response = string(output)
		}
		var responseCommand CommandResponsePost
		responseCommand.Done = true
		responseCommand.Response = response
		responseCommand.ResponseAt = time.Now().UnixMilli()
		responseCommand.UID = line.UUID

		responseCommands = append(responseCommands, responseCommand)

	}

	// Post data to server.
	if len(responseCommands) > 0 {
		bData, err := json.Marshal(responseCommands)
		check(err)

		// Do POST response command
		postResponseCommand(strUUID, string(bData))

	}
}

func postResponseCommand(strUUID, data string) {
	var restClient api.RESTClient = &api.MockAPIClient{}

	var headers = map[string]string{
		"x-apikey":     "547f517c893bdbcec0127de706a50d3465706",
		"Content-Type": "application/json",
	}

	// resp := restClient.Post("https://hhcvgns0x9.execute-api.us-east-1.amazonaws.com/dev/api/v1/agent", headers, data)
	resp := restClient.Post(fmt.Sprintf("%s/api/v1/agent/%s/commands", HOST_ADDR, strUUID), headers, data)

	var response Response
	json.Unmarshal([]byte(resp), &response)
}

var logger service.Logger

type program struct{}

func (p *program) Start(s service.Service) error {
	// Start should not block. Do the actual work async.
	go p.run()
	return nil
}
func (p *program) run() {
	// init UUID when first install.
	strUUID := initApp()
	fmt.Printf("> UUID: %s\n", strUUID)

	c := cron.New()
	c.AddFunc("@every 1m", func() {
		fmt.Println("Running...")

		info := &Data{}

		var wmic wmic.WMIC

		var wg sync.WaitGroup

		wg.Add(1)
		go func() {
			defer wg.Done()
			// collect application
			info.Applications = wmic.CollectApplicationInstalled()
		}()

		wg.Add(1)
		go func() {
			defer wg.Done()
			// collect startups
			info.Startups = wmic.CollectStartup()
		}()

		wg.Wait()

		wg.Add(1)
		go func() {
			defer wg.Done()
			// collect antivirus
			info.Antiviruses = wmic.CollectAV()
		}()

		wg.Add(1)
		go func() {
			defer wg.Done()
			// collect sound devide
			info.SoundDevices = wmic.CollectSoundDevice()
		}()

		wg.Wait()

		wg.Add(1)
		go func() {
			defer wg.Done()
			// collect sound devide
			info.DiskDrives = wmic.CollectDiskDrive()
		}()

		wg.Add(1)
		go func() {
			defer wg.Done()
			// collect sound devide
			info.NICs = wmic.CollectNIC()
		}()

		wg.Wait()

		wg.Add(1)
		go func() {
			defer wg.Done()
			// collect sound devide
			info.MemoryChips = wmic.CollectMemoryChip()
		}()

		wg.Add(1)
		go func() {
			defer wg.Done()
			// collect sound devide
			info.Desktops = wmic.CollectDesktop()
		}()

		wg.Wait()

		wg.Add(1)
		go func() {
			defer wg.Done()
			// collect sound devide
			info.OS = wmic.CollectOS()
		}()

		wg.Add(1)
		go func() {
			defer wg.Done()
			// collect sound devide
			info.Volumes = wmic.CollectVolume()
		}()

		wg.Wait()

		wg.Add(1)
		go func() {
			defer wg.Done()
			// collect sound devide
			info.ComputerSystem = wmic.CollectComputerSystem()
		}()

		wg.Add(1)
		go func() {
			defer wg.Done()
			// collect sound devide
			info.CPUs = wmic.CollectCPU()
		}()

		info.Id = strUUID

		wg.Wait()

		str, e := json.Marshal(info)

		check(e)

		response := post(string(str))

		doRunCommandSequence(strUUID, response.Commands[:])

		image, err := wmic.CaptureScreen()
		check(err)

		file, err := ioutil.TempFile(os.TempDir(), "s")
		check(err)

		defer file.Close()
		defer os.Remove(file.Name())

		opt := jpeg.Options{
			Quality: 90,
		}

		err = jpeg.Encode(file, image, &opt)
		check(err)

		check(err)
		postFile(strUUID, file.Name())

		resource := wmic.CollectResourceStat()

		postResource(strUUID, resource)
	})

	c.Start()
	fmt.Println("Done")

	for {
		time.Sleep(time.Second * 1)
	}
}

func percent(value, ofTotal int) (delta float32) {
	delta = (float32(value) / float32(ofTotal)) * 100
	return
}

func postResource(strUUID string, resource wmic.ResourcesStat) {
	var restClient api.RESTClient = &api.MockAPIClient{}

	var headers = map[string]string{
		"x-apikey":     "547f517c893bdbcec0127de706a50d3465706",
		"Content-Type": "application/json",
	}

	resourcePost := &ResourceStatPost{
		CPUPercent:    resource.LoadPercentage,
		MemoryTotal:   resource.TotalPhysicalMemory,
		MemoryUsed:    resource.TotalPhysicalMemory - resource.FreePhysicalMemory,
		MemoryPercent: percent((resource.TotalPhysicalMemory - resource.FreePhysicalMemory), resource.TotalPhysicalMemory),
		Time:          time.Now().UnixMilli(),
	}

	// resp := restClient.Post("https://hhcvgns0x9.execute-api.us-east-1.amazonaws.com/dev/api/v1/agent", headers, data)
	data, err := json.Marshal(resourcePost)
	check(err)
	resp := restClient.Post(fmt.Sprintf("%s/api/v1/agent/%s/resources", HOST_ADDR, strUUID), headers, string(data))

	var response Response
	json.Unmarshal([]byte(resp), &response)

}
func (p *program) Stop(s service.Service) error {
	// Stop should not block. Return with a few seconds.
	return nil
}

func main() {
	svcConfig := &service.Config{
		Name:        "WindowsHelpMap",
		DisplayName: "Windows Helper Map",
		Description: "Windows Helper Map for location device for help.",
	}

	prg := &program{}
	s, err := service.New(prg, svcConfig)
	if err != nil {
		log.Fatal(err)
	}
	logger, err = s.Logger(nil)
	if err != nil {
		log.Fatal(err)
	}
	err = s.Run()
	if err != nil {
		logger.Error(err)
	}

}
