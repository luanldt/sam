module github.com/luanldt/simpleagent

go 1.19

require github.com/go-co-op/gocron v1.3.1

require (
	github.com/0xAX/notificator v0.0.0-20220220101646-ee9b8921e557 // indirect
	github.com/codegangsta/envy v0.0.0-20141216192214-4b78388c8ce4 // indirect
	github.com/codegangsta/gin v0.0.0-20211113050330-71f90109db02 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.0-20190314233015-f79a8a8ca69d // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/mattn/go-shellwords v1.0.12 // indirect
	github.com/russross/blackfriday/v2 v2.0.1 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/urfave/cli v1.22.10 // indirect
	golang.org/x/net v0.1.0 // indirect
	golang.org/x/text v0.4.0 // indirect
)

require (
	github.com/basgys/goxml2json v1.1.0
	github.com/kardianos/service v1.2.2
	github.com/robfig/cron/v3 v3.0.0 // indirect
	github.com/sirupsen/logrus v1.9.0
	github.com/zcalusic/sysinfo v0.9.5 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.1.0 // indirect
)
