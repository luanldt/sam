$(() => {
    var $dtbAgent = $("#dtb_agent")
    $dtbAgent.DataTable({
        ajax: API_AGENT_DATA,
        columns: [
            {
                data: 'uuid',
                render(data, type, row, meta) {
                    return meta.row + 1
                }
            },
            {
                data: 'uuid',
                render(data) {
                    if (data && data.length > 10) {
                        return `<span data-toggle="tooltip" data-placement="top" title="${data}">${data.substr(0, 8)} . . . ${data.substr(data.length - 8, data.length)}</span>`
                    }
                }
            },
            { data: 'name' },
            { data: 'username' },
            { data: 'os' },
            {
                data: 'status',
                render(data, type, row, meta) {
                    var clzzName, txt
                    switch (data) {
                        case 'A':
                            clzzName = 'success'
                            txt = 'Active'
                            break
                        case 'I':
                            clzzName = 'warning'
                            txt = 'Aways'
                            break
                        case 'D':
                            clzzName = 'dark'
                            txt = 'Disabled'
                            break
                        default:
                            clzzName = 'secondary'
                            txt = 'Unknown'
                    }
                    return `<span class="badge bg-${clzzName}">${txt}</span>`
                }
            },
            { data: 'lastUpdated' },
            {
                data: 'uuid',
                render(data, type, row, meta) {
                    return `<div class="btn-group">
                        <button type="button" ondblclick="" class="btn btn-sm btn-info dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                            Action
                        </button>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="/agents/${data}/detail">View Info</a></li>
                            <li><a class="dropdown-item" href="#">Send Command</a></li>
                            <li><a class="dropdown-item" href="#">Reboot</a></li>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="#">Delete</a></li>
                        </ul>
                    </div>`
                }
            }
        ]
    })
})