import mongoose from "mongoose"

const COLLECTION_NAME = "Agent";

const Application = mongoose.Schema({
    assignmentType: { type: String }, // uint16
    caption: { type: String },
    description: { type: String },
    identifyingNumber: { type: String },
    installDate: { type: String },
    installDate2: { type: String },
    installLocation: { type: String },
    installState: { type: String },  //int16
    helpLink: { type: String },
    helpTelephone: { type: String },
    installSource: { type: String },
    language: { type: String },
    localPackage: { type: String },
    name: { type: String },
    packageCache: { type: String },
    packageCode: { type: String },
    packageName: { type: String },
    productID: { type: String },
    regOwner: { type: String },
    regCompany: { type: String },
    skuNumber: { type: String },
    transforms: { type: String },
    urlInfoAbout: { type: String },
    urlUpdateInfo: { type: String },
    vendor: { type: String },
    wordCount: { type: String }, // uint32
    version: { type: String },
});

const Antivirus = mongoose.Schema({
    displayName: { type: String },
    instanceGuid: { type: String },
    pathToSignedProductExe: { type: String },
    pathToSignedReportingExe: { type: String },
    productState: { type: String },
    timestamp: { type: String }
});

const Startup = mongoose.Schema({
    caption: { type: String },
    command: { type: String },
    description: { type: String },
    location: { type: String },
    name: { type: String },
    settingID: { type: String },
    user: { type: String },
    userSID: { type: String }
});

const NIC = mongoose.Schema({
    adapterType: { type: String },
    adapterTypeId: { type: String },
    autoSense: { type: String },
    availability: { type: String },
    caption: { type: String },
    configManagerErrorCode: { type: String },
    configManagerUserConfig: { type: String },
    creationClassName: { type: String },
    description: { type: String },
    deviceID: { type: String },
    errorCleared: { type: String },
    errorDescription: { type: String },
    guid: { type: String },
    index: { type: String },
    installDate: { type: String },
    installed: { type: String },
    interfaceIndex: { type: String },
    lastErrorCode: { type: String },
    macAddress: { type: String },
    manufacturer: { type: String },
    maxNumberControlled: { type: String },
    maxSpeed: { type: String },
    name: { type: String },
    netConnectionID: { type: String },
    netConnectionStatus: { type: String },
    netEnabled: { type: String },
    networkAddresses: { type: String },
    permanentAddress: { type: String },
    physicalAdapter: { type: String },
    pnpDeviceID: { type: String },
    powerManagementCapabilities: { type: String },
    powerManagementSupported: { type: String },
    productName: { type: String },
    serviceName: { type: String },
    speed: { type: String },
    status: { type: String },
    statusInfo: { type: String },
    systemCreationClassName: { type: String },
    systemName: { type: String },
    timeOfLastReset: { type: String }
})

const DiskDrive = mongoose.Schema({
    availability: { type: String },
    bytesPerSector: { type: String },
    capabilities: { type: String },
    capabilityDescriptions: { type: String },
    caption: { type: String },
    compressionMethod: { type: String },
    configManagerErrorCode: { type: String },
    configManagerUserConfig: { type: String },
    creationClassName: { type: String },
    defaultBlockSize: { type: String },
    description: { type: String },
    deviceID: { type: String },
    errorCleared: { type: String },
    errorDescription: { type: String },
    errorMethodology: { type: String },
    firmwareRevision: { type: String },
    index: { type: String },
    installDate: { type: String },
    interfaceType: { type: String },
    lastErrorCode: { type: String },
    manufacturer: { type: String },
    maxBlockSize: { type: String },
    maxMediaSize: { type: String },
    mediaLoaded: { type: String },
    mediaType: { type: String },
    minBlockSize: { type: String },
    model: { type: String },
    name: { type: String },
    needsCleaning: { type: String },
    numberOfMediaSupported: { type: String },
    partitions: { type: String },
    pnpDeviceID: { type: String },
    powerManagementCapabilities: { type: String },
    powerManagementSupported: { type: String },
    scsiBus: { type: String },
    scsiLogicalUnit: { type: String },
    scsiPort: { type: String },
    scsiTargetId: { type: String },
    sectorsPerTrack: { type: String },
    serialNumber: { type: String },
    signature: { type: String },
    size: { type: String },
    status: { type: String },
    statusInfo: { type: String },
    systemCreationClassName: { type: String },
    systemName: { type: String },
    totalCylinders: { type: String },
    totalHeads: { type: String },
    totalSectors: { type: String },
    totalTracks: { type: String },
    tracksPerCylinder: { type: String }
});

const SoundDevice = mongoose.Schema({
    availability: { type: String },
    caption: { type: String },
    configManagerErrorCode: { type: String },
    configManagerUserConfig: { type: String },
    creationClassName: { type: String },
    description: { type: String },
    deviceID: { type: String },
    dmaBufferSize: { type: String },
    errorCleared: { type: String },
    errorDescription: { type: String },
    installDate: { type: String },
    lastErrorCode: { type: String },
    manufacturer: { type: String },
    mpu401Address: { type: String },
    name: { type: String },
    pnpDeviceID: { type: String },
    powerManagementCapabilities: { type: String },
    powerManagementSupported: { type: String },
    productName: { type: String },
    status: { type: String },
    statusInfo: { type: String },
    systemCreationClassName: { type: String },
    systemName: { type: String }
});

const ComputerSystem = mongoose.Schema({
    "adminPasswordStatus": { type: String },
    "automaticManagedPagefile": { type: String },
    "automaticResetBootOption": { type: String },
    "automaticResetCapability": { type: String },
    "bootOptionOnLimit": { type: String },
    "bootOptionOnWatchDog": { type: String },
    "bootROMSupported": { type: String },
    "bootStatus": { type: String },
    "bootupState": { type: String },
    "caption": { type: String },
    "chassisBootupState": { type: String },
    "chassisSKUNumber": { type: String },
    "creationClassName": { type: String },
    "currentTimeZone": { type: String },
    "daylightInEffect": { type: String },
    "description": { type: String },
    "dnsHostName": { type: String },
    "domain": { type: String },
    "domainRole": { type: String },
    "enableDaylightSavingsTime": { type: String },
    "frontPanelResetStatus": { type: String },
    "hypervisorPresent": { type: String },
    "infraredSupported": { type: String },
    "initialLoadInfo": { type: String },
    "installDate": { type: String },
    "keyboardPasswordStatus": { type: String },
    "lastLoadInfo": { type: String },
    "manufacturer": { type: String },
    "model": { type: String },
    "name": { type: String },
    "nameFormat": { type: String },
    "networkServerModeEnabled": { type: String },
    "numberOfLogicalProcessors": { type: String },
    "numberOfProcessors": { type: String },
    "oemLogoBitmap": { type: String },
    "oemStringArray": { type: String },
    "partOfDomain": { type: String },
    "pauseAfterReset": { type: String },
    "pcSystemType": { type: String },
    "pcSystemTypeEx": { type: String },
    "powerManagementCapabilities": { type: String },
    "powerManagementSupported": { type: String },
    "powerOnPasswordStatus": { type: String },
    "powerState": { type: String },
    "powerSupplyState": { type: String },
    "primaryOwnerContact": { type: String },
    "primaryOwnerName": { type: String },
    "resetCapability": { type: String },
    "resetCount": { type: String },
    "resetLimit": { type: String },
    "roles": { type: String },
    "status": { type: String },
    "supportContactDescription": { type: String },
    "systemFamily": { type: String },
    "systemSKUNumber": { type: String },
    "systemStartupDelay": { type: String },
    "systemStartupOptions": { type: String },
    "systemStartupSetting": { type: String },
    "systemType": { type: String },
    "thermalState": { type: String },
    "totalPhysicalMemory": { type: String },
    "userName": { type: String },
    "wakeUpType": { type: String },
    "workgroup": { type: String }
});

const OS = mongoose.Schema({
    "bootDevice": { type: String },
    "buildNumber": { type: String },
    "buildType": { type: String },
    "caption": { type: String },
    "codeSet": { type: Date },
    "countryCode": { type: String },
    "creationClassName": { type: String },
    "csCreationClassName": { type: String },
    "csdVersion": { type: String },
    "csName": { type: String },
    "currentTimeZone": { type: String },
    "dataExecutionPrevention32BitApplications": { type: String },
    "dataExecutionPreventionAvailable": { type: String },
    "dataExecutionPreventionDrivers": { type: String },
    "dataExecutionPreventionSupportPolicy": { type: String },
    "debug": { type: String },
    "description": { type: String },
    "distributed": { type: String },
    "encryptionLevel": { type: String },
    "foregroundApplicationBoost": { type: String },
    "freePhysicalMemory": { type: String },
    "freeSpaceInPagingFiles": { type: String },
    "freeVirtualMemory": { type: String },
    "installDate": { type: String },
    "largeSystemCache": { type: String },
    "lastBootUpTime": { type: String },
    "localDateTime": { type: String },
    "locale": { type: Date },
    "manufacturer": { type: String },
    "maxNumberOfProcesses": { type: String },
    "maxProcessMemorySize": { type: String },
    "muiLanguages": { type: String },
    "name": { type: String },
    "numberOfLicensedUsers": { type: String },
    "numberOfProcesses": { type: String },
    "numberOfUsers": { type: String },
    "operatingSystemSKU": { type: String },
    "organization": { type: String },
    "osArchitecture": { type: String },
    "osLanguage": { type: Date },
    "osProductSuite": { type: String },
    "osType": { type: String },
    "otherTypeDescription": { type: String },
    "paeEnabled": { type: String },
    "plusProductID": { type: String },
    "plusVersionNumber": { type: String },
    "portableOperatingSystem": { type: String },
    "primary": { type: String },
    "productType": { type: String },
    "registeredUser": { type: String },
    "serialNumber": { type: String },
    "servicePackMajorVersion": { type: String },
    "servicePackMinorVersion": { type: String },
    "sizeStoredInPagingFiles": { type: String },
    "status": { type: String },
    "suiteMask": { type: String },
    "systemDevice": { type: String },
    "systemDirectory": { type: String },
    "systemDrive": { type: String },
    "totalSwapSpaceSize": { type: String },
    "totalVirtualMemorySize": { type: String },
    "totalVisibleMemorySize": { type: String },
    "version": { type: String },
    "windowsDirectory": { type: String }
});

const rootSchema = mongoose.Schema({
    _id: {
        require: true,
        type: String,
    },
    applications: {
        type: [Application],
    },
    antiviruses: {
        type: [Antivirus]
    },
    startups: {
        type: [Startup]
    },
    nics: {
        type: [NIC]
    },
    diskDrives: {
        type: [DiskDrive]
    },
    soundDevices: {
        type: [SoundDevice]
    },
    computerSystem: {
        type: ComputerSystem
    },
    os: {
        type: OS
    },
    cpus: {
        "type": [
            "Mixed"
        ]
    },
    desktops: {
        "type": [
            "Mixed"
        ]
    },
    volumes: {
        "type": [
            "Mixed"
        ]
    },
    lastScreenshoots: {
        type: ["Mixed"]
    },
    status: {
        type: mongoose.SchemaTypes.String
    },
    lastUpdated: {
        type: mongoose.SchemaTypes.Date
    },
}, {
    timestamps: { createdAt: 'created_at', updated_at: 'updated_at' }
});

module.exports = mongoose.model(COLLECTION_NAME, rootSchema)