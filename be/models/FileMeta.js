const mongoose = require("mongoose")

const COLLECTION_NAME = "FileMeta";

const rootSchema = mongoose.Schema({
    name: {
        require: true,
        type: String
    },
    filePath: {
        require: true,
        type: String
    },
    fileSizeInBytes: {
        require: true,
        type: Number
    },
    mimetype: {
        require: true,
        type: String
    }
});

module.exports = mongoose.model(COLLECTION_NAME, rootSchema)