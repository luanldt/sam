const mongoose = require("mongoose")

const COLLECTION_NAME = "ResourcesUsage";

const rootSchema = mongoose.Schema({
    _id: {
        require: true,
        type: String
    },
    resources: [
        {
            time: Date,
            cpu: {
                percent: Number
            },
            memory: {
                percent: Number,
                total: Number,
                usaged: Number,
            },
        }
    ]
});

module.exports = mongoose.model(COLLECTION_NAME, rootSchema)