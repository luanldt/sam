const mongoose = require("mongoose")

const COLLECTION_NAME = "Command"

const rootSchema = mongoose.Schema({
    agentUid: { type: String },
    command: { type: String },
    sendAgentAt: { type: Date },
    response: { type: String },
    responseAt: { type: Date },
    done: { type: Boolean }
}, {
    timestamps: { createdAt: 'created_at'}
})

module.exports = mongoose.model(COLLECTION_NAME, rootSchema)