const express = require('express');
const Agent = require('../models/Agent');
const Command = require('../models/Command');
const FileMeta = require('../models/FileMeta');
const ResourcesUsage = require('../models/ResourcesUsage')
const router = express.Router();
const multer = require('multer');
const crypto = require('crypto')
const fs = require('fs')
const os = require('os');
const { default: mongoose } = require('mongoose');
import { PATH_FILE_DISK } from '../lib/contants'


const postAgent = async function (req, res, next) {

    var body = req.body;

    if (!body._id) {
        res.status(404).send()
    }

    body = {
        ...body,
        status: 'A',
        lastUpdated: new Date()
    }

    await Agent.findOneAndUpdate(
        {
            _id: body._id,
        },
        body,
        {
            new: true,
            upsert: true
        }
    )

    const commands = await Command.find({
        agentUid: body._id,
        done: false
    }).select('_id command')

    var resBody = {
        commands: []
    }

    if (commands) {
        resBody.commands = commands.map(t => ({
            command: t.command,
            uuid: t._id
        }))
    }

    res.status(200).json(resBody);
}


const postScreenshoot = async (req, res, next) => {
    const { id } = req.params;
    if (!id) {
        res.sendStatus(404)
        return
    }

    // check input file is empty?
    if (!req.files) {
        res.sendStatus(400)
        return
    }

    const agent = await Agent.findById(id)
    if (!agent) {
        res.sendStatus(404)
        return
    }
    const { lastScreenshoots } = agent
    if (lastScreenshoots) {
        lastScreenshoots.forEach(async element => {
            const fileMeta = await FileMeta.findById(element)

            if (!fileMeta) {
                return
            }

            if (fileMeta.filePath) {
                fs.unlinkSync(fileMeta.filePath)
            }
            await fileMeta.remove()
        });
    }

    const fileMetas = []
    req.files.map(file => {
        const uuid = crypto.randomUUID()
        const path = `${PATH_FILE_DISK}/screenshoots/${id}_${uuid}_${file.filename}.jpg`
        fs.copyFileSync(file.path, path)
        const objectId = mongoose.Types.ObjectId()

        var fileMeta = new FileMeta({
            _id: objectId,
            name: file.filename,
            filePath: path,
            fileSizeInBytes: file.size,
            mimetype: file.mimetype
        })
        fileMetas.push(fileMeta)
    })

    const opsRes = await FileMeta.bulkSave(fileMetas)
    if (opsRes.insertedCount > 0) {
        const screenshoots = []
        opsRes.getInsertedIds().forEach(insertedId => {
            screenshoots.push(insertedId._id)
        })
        agent.lastScreenshoots = screenshoots
        await agent.save()
    }


    res.status(200).json('ok')
}

// @flow
async function postResourceStat(req: express.Request, res: express.Response, next: express.NextFunction) {
    /*
    {
        "time": new Date(),
        "cpuPercent": 30,
        "memoryPercent": 64,
        "memoryTotal": 8589934592,
        "memoryUsed": 5497558138,
    }
     */
    const body = req.body
    const uuid = req.params.id
    await ResourcesUsage.findOneAndUpdate(
        { _id: uuid },
        {
            $push: {
                "resources": {
                    "time": body.time,
                    "cpu": {
                        percent: body.cpuPercent
                    },
                    "memory": {
                        percent: body.memoryPercent,
                        total: body.memoryTotal,
                        usaged: body.memoryUsed,
                    },
                }
            }
        },
        { new: true, upsert: true }
    )

    res.sendStatus(201)

}

async function postResponseCommand(req: Request, res: Response, next: express.NextFunction) {
    /*
    [
        {
            "uid": "command_uid_1",
            "response": "OK",
            "responseAt": new Date(),
            "done": true
        },
        {
            "uid": "command_uid_2",
            "response": "OK",
            "responseAt": new Date(),
            "done": true
        },
    ]
    */

    const body = req.body
    const uuid = req.params.id

    body.forEach(async command => {
        await Command.findOneAndUpdate({ _id: command.uid }, {
            response: command.response,
            responseAt: command.responseAt,
            done: command.done
        })
    })

    res.sendStatus(200)
}

/* POST save payload agent */
router.post('/', postAgent);
router.post('/:id/screenshoot', multer({ dest: os.tmpdir() }).any(), postScreenshoot)
router.post('/:id/resources', postResourceStat)
router.post('/:id/commands', postResponseCommand)


module.exports = router;
