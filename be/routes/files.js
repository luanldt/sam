const express = require('express');
const router = express.Router();
const FileMeta = require('../models/FileMeta')

router.get('/:rdname/:uid', async (req, res, next) => {
    const { rdname, uid } = req.params
    const { sig, exp, y2q } = req.query

    console.log(rdname, uid)

    if (!rdname || !uid) {
        res.sendStatus(404)
    }

    const fileMeta = await FileMeta.findById(uid)
    if (!fileMeta) {
        res.sendStatus(404)
    }

    res.header("Content-Disposition", getContentDisposition(y2q, fileMeta.name))
    res.sendFile(fileMeta.filePath)

});

const getContentDisposition = (y2q, fileName) => {
    if (!y2q || y2q == "aW5saW5l") {
        return "inline"
    }

    return `attachment; filename="${fileName}"`
}

module.exports = router;