const express = require('express');
const Agent = require('../../models/Agent');
const Command = require('../../models/Command');
const router = express.Router();

/* POST save payload agent */
router.get('/', async (req, res, next) => {
    const agents = await (await Agent.find().select("_id computerSystem.name computerSystem.userName os.caption status lastUpdated")).map(doc => {
        return {
            uuid: doc._id,
            name: doc.computerSystem.name,
            username: doc.computerSystem.userName,
            os: doc.os.caption,
            status: doc.status,
            lastUpdated: doc.lastUpdated
        }
    })

    res.json({ data: agents })
});

/* POST save command run on agent */
router.post('/:id/commands', async (req, res, next) => {
    const body = req.body

    const command = new Command({
        ...body,
        agentUid: req.params.id,
        createdAt: new Date(),
        done: false,
    })
    
    await command.save()

    res.status(201).json(command)
})

/* GET command */
router.get('/:id/commands', async (req, res, next) => {
    const agentUid = req.params.id
    const { skip, limit } = req.query

    const count = await Command.countDocuments({agentUid})

    const commands = await Command.find({agentUid})
        .skip(skip)
        .limit(limit)

    const hasNext = ((skip + limit) < count)

    const cursor = btoa(`${skip + limit}:${limit}:${hasNext}`)

    res.header('cursor', cursor)
        .json(commands)

})

module.exports = router;
