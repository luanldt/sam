import express from 'express'
import Agent from '../models/Agent'
import FileMeta from '../models/FileMeta'
import crypto from 'crypto'
import Command from '../models/Command'
import mongoose from "mongoose"
import ResourceUsage from '../models/ResourcesUsage'
import { TITLE } from '../lib/contants'

const router = express.Router();

/* GET home page. */
router.get('/', async function (req, res, next) {
  res.render('index', { title: 'Dashboard' });
});

/* GET basic table. */
router.get('/table/basic-table', async function (req, res, next) {
  res.render('basic-table', { title: 'Basic Table' });
});

/* GET datatables. */
router.get('/table/datatable', async function (req, res, next) {
  res.render('datatable', { title: 'Data Tables' });
});

/* GET calendar. */
router.get('/calendar', async function (req, res, next) {
  res.render('calendar', { title: 'Calendar' });
});

/* GET charts. */
router.get('/charts', async function (req, res, next) {
  res.render('charts', { title: 'Charts' });
});

/* GET chat. */
router.get('/chat', async function (req, res, next) {
  res.render('chat', { title: 'Chat' });
});

/* GET compose. */
router.get('/compose', async function (req, res, next) {
  res.render('compose', { title: 'Compose' });
});

/* GET ui. */
router.get('/ui', async function (req, res, next) {
  res.render('ui', { title: 'UI' });
});

/* GET email. */
router.get('/email', async function (req, res, next) {
  res.render('email', { title: 'Email' });
});

/* GET forms. */
router.get('/forms', async function (req, res, next) {
  res.render('forms', { title: 'Forms' });
});

/* GET signin. */
router.get('/signin', async function (req, res, next) {
  res.render('signin', { title: 'Signin', layout: false });
});

/* GET signup. */
router.get('/signup', async function (req, res, next) {
  res.render('signup', { title: 'Signup', layout: false });
});

/* GET 404. */
router.get('/404', async function (req, res, next) {
  res.render('404', { title: '404', layout: false });
});

/* GET 500. */
router.get('/500', async function (req, res, next) {
  res.render('500', { title: '500', layout: false });
});

/* GET blank. */
router.get('/blank', async function (req, res, next) {
  res.render('blank', { title: 'Blank' });
});

/* GET maps/google-map. */
router.get('/maps/google-maps', async function (req, res, next) {
  res.render('google-maps', { title: 'Google Maps' });
});

/* GET maps/vector-maps */
router.get('/maps/vector-maps', async function (req, res, next) {
  res.render('vector-maps', { title: 'Vector Maps' })
})

router.get('/agents/:id', async function (req, res, next) {

  const agent = await Agent.findById(req.params.id).select("-__v")

  res.json(agent)
})

router.get('/agents', async (req, res, next) => {
  res.render('agent', {
    title: TITLE,
    apiAgentData: '/api/internal/v1/agents'
  })
})

function buildURL(screenshoot: String) {
  const uuidRand = crypto.randomUUID()
  return `/file/${uuidRand}/${screenshoot}?sig=${uuidRand.substring(12, 20)}&y2q=aW5saW5l&ref=${uuidRand.substring(8, 15)}&exp=${new Date().getTime()}}}`
}

router.get('/agents/:uuid/detail', async (req, res, next) => {
  const agent = await Agent.findById(req.params.uuid)
  if (!agent) { res.sendStatus(404); return; }

  // build screenshoots
  const screenshoots = agent.lastScreenshoots
    .map(screenshoot => {
      return buildURL(screenshoot)
    })

  // build first 5 commands 
  const countCommand = await Command.countDocuments({ agentUid: req.params.uuid })
  const commands = await Command.find({ agentUid: req.params.uuid })
    .sort({ createdAt: -1 })
    .limit(5)

  // build cursor for the command load 
  const cursor = btoa(`${5}:${10}:${5 < countCommand}`)
  commands.sort((a, b) => {
    if (a.createdAt < b.createdAt) {
      return -1
    }

    if (a.createdAt > b.createdAt) {
      return 1
    }

    return 0
  })

  // build the data for the CharJS in the agent view detail for resource stat
  const resourceUsaged = await ResourceUsage.findById(req.params.uuid)
    .slice("resources", [0, 20]).select("-_id")

  res.render('agent-detail', {
    title: agent.computerSystem.name,
    apiSendCommand: `/api/internal/v1/agents/${req.params.uuid}/commands`,
    apiGetCommand: `/api/internal/v1/agents/${req.params.uuid}/commands?cursor=${cursor}`,
    rec: agent,
    screenshoots,
    commands,
    resourceUsaged: JSON.stringify(resourceUsaged)
  })
})

module.exports = router;
