import mongoose from 'mongoose'
import request from 'supertest'
import app from '../app'
import ResourcesUsage from '../models/ResourcesUsage'

describe('Agent Router Test /api/v1/agent', function () {
    test('response to /:id/resources', () => {
        request(app).post('/api/v1/agent/agent-id-1/resources')
            .send({
                time: new Date().getTime(), cpuPercent: 30, memoryPercent: 52,
                "memoryTotal": 8589934592,
                "memoryUsed": 5497558138,
            }).expect(201)

        request(app).post('/api/v1/agent/agent-id-1/resources')
            .send({
                time: new Date().getTime(), cpuPercent: 30, memoryPercent: 52,
                "memoryTotal": 8589934592,
                "memoryUsed": 5497558138,
            }).expect(201)
    })
})

async function clearResourcesTest() {
    await ResourcesUsage.deleteMany({_id: 'agent-id-1'})
}

afterAll(() => {
  return clearResourcesTest();
});