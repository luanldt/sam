function define(name, value) {
    Object.defineProperty(exports, name, {
        value,
        enumerable: true
    })
}

/* DEFINE CONST */
define("TITLE", "Samagent")
define("PATH_FILE_DISK", "/Volumes/Data/mnt/sam-sam")