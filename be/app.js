var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var serverless = require('serverless-http')
var multer = require('multer')
var hbs = require('hbs')

const mongoose = require('mongoose')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var agentRouter = require('./routes/agent');
var fileRouter = require('./routes/files');
var internalAgentRouter = require('./routes/internal/agents');

var app = express();

hbs.registerHelper('eachProperty', function (context, options) {
  var ret = "";
  if (!context) return ""
  const obj = context.toObject ? context.toObject() : context
  for (var prop in obj) {
    // if (context.hasOwnProperty(prop)) {
    ret = ret + options.fn({ property: prop, value: obj[prop] });
    // }
  }
  return ret;
});

hbs.registerHelper('formatDate', (context, options) => {
  const date = new Date(context)
  return date.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
})

hbs.registerHelper('getAgoDay', (context, options) => {
  const date = new Date(context)
  const currentDate = new Date()
  var seconds = (currentDate - date) / 1000

  var interval = seconds / 31536000;

  if (interval > 1) {
    return Math.floor(interval) + " years ago";
  }
  interval = seconds / 2592000;
  if (interval > 1) {
    return Math.floor(interval) + " months ago";
  }
  interval = seconds / 86400;
  if (interval > 1) {
    return Math.floor(interval) + " days ago";
  }
  interval = seconds / 3600;
  if (interval > 1) {
    return Math.floor(interval) + " hours ago";
  }
  interval = seconds / 60;
  if (interval > 1) {
    return Math.floor(interval) + " minutes ago";
  }
})

// helper build the status of the agent
hbs.registerHelper('buildStatus', (context, options) => {
  var clzzName, txt
  switch (context) {
    case 'A':
      clzzName = 'success'
      txt = 'Active'
      break
    case 'I':
      clzzName = 'warning'
      txt = 'Aways'
      break
    case 'D':
      clzzName = 'dark'
      txt = 'Disabled'
      break
    default:
      clzzName = 'secondary'
      txt = 'Unknown'
  }
  return txt
})

hbs.registerPartials(path.join(__dirname, '/views/partials'), {
  rename: function (name) {
    return name;
    // all non-word characters replaced with underscores
    // return name.replace(/\W/g, '_')
  }
})

// view engine setup
app.engine('hbs', hbs.__express)
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.set('view options', { layout: 'layouts/main' })

app.use(logger('dev'));
app.use(express.json({ limit: '50MB' }));
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api/v1/agent', agentRouter);
app.use('/file', fileRouter);
app.use('/api/internal/v1/agents', internalAgentRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  // res.render('error');
  res.json(err)
});

// connect mongoose
// mongoose.connect('mongodb+srv://app_user_001:SmCStNgWRUmAjrLi@atlascluster.ry9w2jq.mongodb.net/sam-sam-is-going?retryWrites=true&w=majority')
mongoose.connect('mongodb://localhost/sam-sam-is-going')
const database = mongoose.connection;

database.on('error', (err) => {
  console.error(err)
});

database.once('connected', () => {
  console.log('Database connected!')
});

// module.exports.handler = serverless(app);
module.exports = app;
